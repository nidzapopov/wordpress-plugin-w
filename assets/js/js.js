/*
* 
* Class main
* Set validation
* Create obj
* Send data through ajax
*
*/

class Main {
    constructor(data, btn) {
        this.data = data;
        this.btn = btn;
        this.validation();
    }

    /*
    * 
    * Validation and create object 
    * 
    * @param object 
    *
    */

    validation() {
        var error = false;
        const obj = {};

        Array.from(this.data).map((element) => {
            if (element.value === '')
                error = true;
            obj[element.getAttribute('data')] = element.value;
        });
        (error) ? alert('validation') : this.send(obj);
    }

    /*
    * 
    * Ajax 
    * POST, form CPT
    * 
    * @param object 
    *
    */

    send(obj) {
        const id = document.querySelectorAll('[data-id]')[0];
        obj.action = 'handle_request';
        obj.id = id.getAttribute('data-id');

        jQuery.post(
            veh_app_script.ajaxurl,
            obj,
            function (data) {
                console.log(data)
                var msg = JSON.parse(data);
                (msg.msg != 'Update success')
                    ? alert('Status - Pregledaj konzolu')
                    : alert('Status - ' + msg.status + ' / ' + 'Msg - ' + msg.msg);

            }
        );
    }
}

/*
* 
* Document ready set event on submit btn 
*
*/

jQuery(document).ready(function () {
    const btn = document.getElementById('submit');
    if (btn) {
        btn.addEventListener("click", function () {
            const data = document.querySelectorAll('[data]');
            new Main(data, this);
        });
    }
})




