<?php

/**
 * Class Data 
 * 
 * ------------------------------------------------------
 * Get taxonomy, cpt, acf data                          -
 * Update data POST by ajax                             -
 * Check user role                                      -
 * ------------------------------------------------------
 * 
 * @since 1.0.0
 */

class Data {

    /**
     * @var object 
     */
    private static $instance = null;

    /**
     * @var int 
     */
    public $id;

    /**
     * @var string 
     */
    public $title;

    /**
     * @var string 
     */
    public $sub_title;

    /**
     * @var string 
     */
    public $location;

    /**
     * @var string 
     */
    public $type;

    /**
     * @var bool 
     */
    public $permission;

    /**
     * 
     * @since 1.0.0
     * 
     * Check user 
     * Set data for form
     * 
     * @param array $post
     * 
     */

    public function __construct ( $post )
    {
        $this->permission = $this->checkUserRole( $post );
        $this->id = $post->ID;
        $this->title = $post->post_title;
        $this->sub_title = get_field('acf_subtitle');

        $this->setTaxonomy();
    }

    /**
     * 
     * @since 1.0.0
     * 
     * Get Taxonomy by cpt id
     * 
     * @return void
     */

    public function setTaxonomy () : void
    {
        $location = wp_get_object_terms( $this->id, 'location' );
        $type = wp_get_object_terms( $this->id, 'type' );

        // if taxonomy empty
        ( count( $location ) === 0 )
            ? $this->location = ''
            : $this->location = $location[0]->name;
        ( empty( $type ) )
            ? $this->type = ''
            : $this->type = $type[0]->name;
    }

    /**
     * 
     * @since 1.0.0
     * 
     * Check User Role and post author
     * 
     * @return bool role
     */

    public function checkUserRole ( $post ) 
    {
        $user = wp_get_current_user();
        $allowed_roles = array( 'administrator', 'author' );

        ( array_intersect( $allowed_roles, $user->roles ) ) 
            ? $role = true 
            : $role = false;
        
        if ( (int)$post->post_author !== (int)$user->ID )
            $role = false;
            
        return $role;
    }

    /**
     * 
     * @since 1.0.0
     * 
     * Update data
     * POST request from ajax
     * Validate data
     * 
     * @return array echo
     */

    static function updateData () 
    {
        // get validation
        $data = new Validation( $_POST, 'cptValidation' );

        // if has errors
        if ( count($data->errors) > 0 ) {
            echo json_encode(['status' => 400, 'msg' => $data->errors]);
            return false;
        }
        
        $id = (int)$data->checkedData["id"];
        $title = $data->checkedData["title"];
        $subtitle = $data->checkedData["subtitle"];
        $location = $data->checkedData["location"];
        $type = $data->checkedData["type"];

        $my_post = array(
            'ID'           => $id,
            'post_title'   => $title,
        );
        
        wp_update_post( $my_post );
        update_field( 'acf_subtitle', $subtitle, $id );
        wp_set_post_terms( $id, $location, 'location', false);
        wp_set_post_terms( $id, $type, 'type', false);

        echo json_encode(['status' => 200, 'msg' => 'Update success']);
        
        wp_die();
    }

    /**
     * 
     * @since 1.0.0
     * 
     * Reset query
     * 
     */

    function __destruct () 
    {
        wp_reset_query();
    }

    /**
     * 
     * @since 1.0.0
     * 
     * @param array $post
     * 
     */

    public static function getInstance ( $post )
    {
        if (self::$instance == null)
            self::$instance = new Data( $post );

        return self::$instance;
    }

}