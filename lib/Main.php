<?php

/**
 * Class Main 
 * Singletone instance
 * 
 * ----------------------------------------------
 * Trigger all required metods                  -
 * Create CPT and Taxonomy                      -
 * Includ all class                             -
 * Register wp filter, action                   -
 * ----------------------------------------------
 * 
 * @since 1.0.0
 */

class Main {

    /**
     * Class Main 
     *
     * @since 1.0.0
     * 
     * @var object
     */

    private static $instance = null;

    /**
     * @since 1.0.0
     *
     * Init all methods 
     * 
     */

    private function __construct ()
    {
        $this->includeFile(); 
        $this->register();
        $this->setSearch();
        $this->createAcfField();
        $this->createCptTaxonomy();
    }

    /**
     * @since 1.0.0
     *
     * Activate plugin
     * 
     */

    public function activate () : void
    {

		flush_rewrite_rules();
    }

    /**
     * @since 1.0.0
     *
     * Deactivate plugin
     * 
     */
    
    public function deactivate () : void
    {
		flush_rewrite_rules();
	}

    /**
     *
     * @since 1.0.0
     *
     * include LIB folder
     * 
     * @return void
     */

    private function includeFile () : void {
        // Loop through all classes in the lib folder and include
        foreach ( glob( plugin_dir_path( __FILE__ ) . "../lib/*.php" ) as $file ) {
            include_once $file;
        }
    }

    /**
     *
     * @since 1.0.0
     *
     * Create CPT and Taxonomy
     * 
     * @return void
     */

    private function createCptTaxonomy () : void
    {
        Taxonomy::getInstance( 'estate', Declaration::${'custom_post_type'}, array(
            'location' => Declaration::${'taxonomy_location'},
            'type' => Declaration::${'taxonomy_type'}
        ));
    }

    /**
     *
     * @since 1.0.0
     * 
     * Set nativ wordpress search 
     * 
     * @return void
     */

    private function setSearch() : void {
        Search::getInstance();
    }

    /**
     *
     * @since 1.0.0
     * 
     * Create acg group, acf text field, acf gallery
     *
     * @return void
     */

    private function createAcfField () : void
    {
        if ( class_exists('acf') ) {
            acf_add_local_field_group(Declaration::${'acf_group'});
            acf_add_local_field(Declaration::${'acf_field_text'});
            acf_add_local_field(Declaration::${'acf_field_gallery'});
        } else {
            add_action( 'template_redirect', array( $this, 'missingAcf' ) );
        }
    }

    /**
     *
     * @since 1.0.0
     * 
     * Needs Advanced Custom Fields Pro
     *
     * @return void
     */

    public function missingAcf () : void 
    {
        $msg = __( 'This website needs "Advanced Custom Fields Pro" to run. Please download and activate it', 'tp-notice-acf' );
        wp_die( $msg );
    }

    /**
     *
     * @since 1.0.0
     *
     * Register template, handle request and enqueue
     * 
     * @return void
     */

    private function register() : void
    {
        register_activation_hook( plugin_dir_url( __FILE__ ), array( $this, 'activate' ) );
        register_deactivation_hook( plugin_dir_url( __FILE__ ), array( $this, 'deactivate' ) );
        add_filter( 'single_template', array( $this, 'includeTemplate' ) );
        add_action( 'wp_ajax_handle_request', array( $this, 'handle_request' ) );
        add_action( 'wp_ajax_nopriv_handle_request', array( $this, 'handle_request' ) );
        add_action( 'wp_enqueue_scripts',  array( $this, 'enqueue' ) );
    }

    /**
     *
     * @since 1.0.0
     * 
     * Enqueue js object, style and js file
     * 
     * @return void
     */

    public function enqueue () : void
    {
        $localize = array(
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        );
        wp_enqueue_style( 'mypluginstyle', plugin_dir_url( __FILE__ ) . '../assets/css/style.css' );
        wp_enqueue_script( 'veh-app-search', plugin_dir_url( __FILE__ ) . '../assets/js/js.js', array( 'jquery' ) );
        wp_localize_script( 'veh-app-search', 'veh_app_script', $localize);
    }

    /**
     *
     * @since 1.0.0
     * 
     * if cpt estate include template single page
     * 
     * @return string template single page.
     */
    
    public function includeTemplate ()
    {
        global $post;
        if ( $post->post_type === 'estate' ) {
            if ( file_exists( plugin_dir_path( __FILE__ ) . '../template/single-estate.php' ) ) 
                return plugin_dir_path( __FILE__ ) . '../template/single-estate.php';
        }
    }

    /**
     *
     * @since 1.0.0
     *
     * Handle request, redirect ajax request to class Data
     *
     * @return void.
     */
    
    public function handle_request () : void
    {
        Data::updateData();
    }

    /**
     *
     * @since 1.0.0
     *
     * @return object
     */

    public static function getInstance ()
    {
        if (self::$instance == null)
            self::$instance = new Main();

        return self::$instance;
    }

}