<?php

/**
 * Class Taxonomy 
 * Singletone class
 * Extends CustomPostType
 * 
 * ----------------------------------------------
 * Set multi taxonomy                           -
 * Create taxonomy and register                 -
 * ----------------------------------------------
 * 
 * @since 1.0.0
 */

class Taxonomy extends CustomPostType {

    /**
     * @var string
     */
    private $taxonomy;
    /**
     * @var object 
     */
    private static $instance = null;

    /**
     * 
     * @since 1.0.0
     * 
     * Instantiate parent
     * 
     * @param string $cpt name
     * @param array $cptDeclaration 
     * @param array $taxonomy
     * 
     */

    public function __construct(string $cpt, array $cptDeclaration, array $taxonomy)
    {
        parent::__construct ( $cpt, $cptDeclaration );
        
        $this->taxonomy = $taxonomy;
        add_action( 'init', array( $this, 'createTaxonomy' ) );
    }

    /**
     *
     * @since 1.0.0
     *
     * @return void
     */

    public function createTaxonomy () : void
    {
        // loop through all taxonomy and send to register
        foreach ( $this->taxonomy as $key => $arr ) {
            register_taxonomy( $key, $this->name, $arr );
        }
    }

    /**
     *
     * @since 1.0.0
     *
     * @return object.
     */

    public static function getInstance ( string $cpt, array $cptDeclaration, array $taxonomy )
    {
        if (self::$instance == null)
            self::$instance = new Taxonomy($cpt, $cptDeclaration, $taxonomy);
    
        return self::$instance;
    }

}