<?php

/**
 * Class CustomPostType 
 * Abstract class
 * 
 * ----------------------------------------------
 * Set name and declare CPT                     -
 * Create CPT and add to register post type     -
 * ----------------------------------------------
 * 
 * @since 1.0.0
 */

abstract class CustomPostType {

    /**
     * @var string
     */

    public $name;

    /**
     * @var array
     */

    private $declaration;

    /**
     * 
     * @since 1.0.0
     * 
     * @param string $name
     * @param array $declaration
     * 
     */

    public function __construct ( string $name, array $declaration )
    {
        $this->name = $name;
        $this->declaration = $declaration;
        add_action( 'init', array( $this, 'createCustomPostType' ) );
    }

    /**
     * 
     * @since 1.0.0
     * 
     * Register post type
     */

    public function createCustomPostType () : void
    {
        register_post_type( $this->name, $this->declaration );
    }

}