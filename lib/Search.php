<?php

/**
 * Class Search 
 * 
 * ----------------------------------------------
 * Set custom search to nativ wp search         -
 * ----------------------------------------------
 * 
 * @since 1.0.0
 */

class Search {

    /**
     * @var null
     */
    private static $instance = null;

    /**
     * 
     * @since 1.0.0
     * 
     */

    public function __construct ()
    {
        $this->addFilter();
    }

    /**
     * 
     * @since 1.0.0
     * 
     * ADD POSTS CLAUSES REQUEST
     * 
     * @return void
     */

    private function addFilter () : void
    {
        add_filter( 'posts_clauses_request', array( $this, 'filterClausesRequest' ), 10, 2 );
    }

    /**
     * 
     * @since 1.0.0
     * 
     * Add filter to nativ wp search
     * 
     * @return array
     */

    public function filterClausesRequest ( $clauses, $wp_query ) 
    {
        global $wpdb;

        if ( ! is_admin() && $wp_query->is_search && isset( $wp_query->query_vars['s'] ) ) {
            $s = $wp_query->query_vars['s'];

            $clauses['join'] .="LEFT JOIN {$wpdb->postmeta} ON {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id 
                                INNER JOIN
                                {$wpdb->term_relationships} ON {$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id
                                INNER JOIN
                                {$wpdb->term_taxonomy} ON {$wpdb->term_taxonomy}.term_taxonomy_id = {$wpdb->term_relationships}.term_taxonomy_id
                                INNER JOIN
                                {$wpdb->terms} ON {$wpdb->terms}.term_id = {$wpdb->term_taxonomy}.term_id";
            
            $clauses['where'] .= " AND {$wpdb->posts}.post_status = 'publish' OR {$wpdb->posts}.post_title LIKE '%$s%' OR {$wpdb->postmeta}.meta_value LIKE '%$s%' 
                                   OR {$wpdb->terms}.name LIKE '%$s%' ";

            $clauses['groupby'] .= "{$wpdb->posts}.ID";
        }
        
        return $clauses;

    }

    /**
     *
     * @since 1.0.0
     *
     * @return object.
     */

    public static function getInstance ()
    {
        if (self::$instance == null)
            self::$instance = new Search();

        return self::$instance;
    }

}