<?php

/**
 * Class Validation 
 * 
 * ------------------------------------------------------
 * Set Validation (required, string) for POST request   -
 * ------------------------------------------------------
 * 
 * @since 1.0.0
 */

class Validation {

    /**
     * @var array
     */
    public $checkedData = [];

    /**
     * @var array
     */
    public $errors = [];

    /**
     * 
     * @since 1.0.0
     * 
     * @param array $post_data all data in post
     * @param array $declaration
     * 
     */

    public function __construct ( $post_data, $declaration )
    {
        $this->post_data = $post_data;
        $this->data = Declaration::${$declaration};
        $this->checkPost();
    }

    /**
     * 
     * @since 1.0.0
     * 
     * Check if every post data is send 
     * 
     * @return void
     */

    private function checkPost () : void
    {
        // loop through $declaration 
        foreach ( $this->data as $key => $value ) { 
            // set error if not post every required data
            if ( !array_key_exists( $key, $this->post_data ) ) 
                $this->errors[] = 'Missing '.$key.' field.';
        }
        $this->checkArray();
    }

    /**
     * 
     * @since 1.0.0
     * 
     * Check post data declaration and skip action
     * 
     * @return void
     */

    private function checkArray () : void
    {
        // loop through POST data 
        foreach ( $this->post_data as $key => $value ) { 
			if ( $key != 'action' ){
				if ( array_key_exists( $key, $this->data ) ) 
                    // send to validation and explode ( 'req|string' )
					$this->checkData( $key, $value, explode( "|", $this->data[$key] ) ); 
			}
        }
    }

    /**
     * 
     * @since 1.0.0
     * 
     * 'title' => 'Some title' 
     * 
     * @param string $key ( title )
     * @param string $value ( Some title )
     * @param array $arr ( req, string )
     * 
     * @return void
     */

    private function checkData ( string $key, string $value, array $arr ) : void
    {
        foreach ($arr as $values) {
            // send to methods like req and string
            $str = $this->{$values}( $key, $value );
            $this->checkedData[$key] = $str;
		}
    }

    /**
     * 
     * @since 1.0.0
     * 
     * @param string $key ( title )
     * @param string $value ( Some title )
     * 
     * @return string
     */

    private function req ( string $key, string $value )
	{
		if($value == '' || $value == null)
			$this->errors[] = 'required '.$key.' field.';
        
		return $value;
    }

    /**
     * 
     * @since 1.0.0 
     * 
     * @param string $key ( title )
     * @param string $value ( Some title )
     * 
     * @return string
     */
    
    private function string ( string $key, string $value )
	{
        if ( is_string( $value ) ) {
            $values = trim($value);
            $values = stripslashes($value);
            $values = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
            return $values;
        } else {
            $this->errors[] = $key.' is not a string';
            return $value;
        }
    }

}