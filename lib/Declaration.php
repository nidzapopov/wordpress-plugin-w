<?php 

/**
 * Class Declaration 
 * 
 * ------------------------------------------------------
 * Cpt, tax, acf validation                             - 
 * Cpt declaration                                      -
 * Taxonomy declaration for location and type           -
 * Acf group declaration                                -
 * Acf field sub_title and gallery                      -
 * ------------------------------------------------------
 * 
 * @since 1.0.0
 */


class Declaration {

    /**
     * Cpt, tax, acf validation 
     * @var array
     */
    static $cptValidation = array(
		'title' => 'req|string', 
		'subtitle' => 'req|string',
		'location' => 'req|string',
		'type' => 'req|string',
		'id' => 'req|string',
	);

	/**
     * Cpt declaration
     * @var array
     */
	static $custom_post_type = array(
		'labels' => array(
            'name' => 'Estates',
            'menu_name' => 'Real estates'
        ),
        'description' => 'Displays real estate',
        'public' => true,
        'supports' => array( 'title' ),
        'has_archive'   => true,
        'hierarchical'  => true,
        'rewrite'       => array('slug' => 'estate/type/real_estate','with_front' => false),
        'query_var'     => true,
    );
    
    /**
     * Taxonomy declaration for location
     * @var array
     */
    static $taxonomy_location = array(
        'label' => 'Location',
        'rewrite' => array( 'slug' => 'location' ),
        'hierarchical' => false,
        'has_archive' => 'courses',
        'show_ui' => true,
        'show_tagcloud' => false,
        'rewrite' => array('slug' => 'location/location','with_front' => false)
    );

    /**
     * Taxonomy declaration for type
     * @var array
     */
    static $taxonomy_type = array(
        'label' => 'Type',
        'rewrite' => array( 'slug' => 'type' ),
        'hierarchical' => false,
        'has_archive' => 'courses',
        'show_ui' => true,
        'show_tagcloud' => false,
        'rewrite' => array('slug' => 'estate/type','with_front' => false)
    );
    
    /**
     * Acf group declaration 
     * @var array
     */
    static $acf_group = array(
        'key' => 'acf',
        'title' => 'Custom fields',
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'estate',
                ),
            ),
        ),
    );

    /**
     * Acf field sub_title 
     * @var array
     */
    static $acf_field_text = array(
        'key' => 'field_acf_subtitle',
        'label' => 'Sub Title',
        'name' => 'acf_subtitle',
        'type' => 'text',
        'parent'=> 'acf',
    );

    /**
     * Acf field gallery 
     * @var array
     */
    static $acf_field_gallery = array(
        'key' => 'field_acf_gallery',
        'label' => 'Gallery',
        'name' => 'acf_gallery',
        'type' => 'gallery',
        'parent'=> 'acf',
    );

}
