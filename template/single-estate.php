<?php   
    get_header();

    /*
    * 
    * @var data->permission ( permission for user role and post author )
    * @var data->id ( post id )
    * @var data->title ( post title )
    * @var data->sub_title ( acf field )
    * @var data->location ( tax location )
    * @var data->type ( tax type )
    *
    */

    $data = Data::getInstance( $post );
?>

<!--
    Update form for CPT, taxonomy, acf field
-->

<?php if ( $data->permission ) : ?>
    <section class="form">
        <div class="container">
            <h1>CPT Form</h1>
            <div class="form-wrap" data-id="<?php echo $data->id; ?>">
                <div class="title">
                    <label>Title cpt</label>
                    <input type="text" data="title" value="<?php echo $data->title; ?>" />
                </div>
                <div class="sub_title">
                    <label>Sub_title acf</label>
                    <input type="text" data="subtitle" value="<?php echo $data->sub_title; ?>" />
                </div>
                <div class="taxonomy-location">
                    <label>Tax location</label>
                    <input type="text" data="location" value="<?php echo $data->location; ?>" />
                </div>
                <div class="taxonomy-type">
                    <label>Tax type</label>
                    <input type="text" data="type" value="<?php echo $data->type; ?>" />
                </div>
                <div class="button-post">
                    <button type="button" id="submit">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </section>
<?php endif;  ?>




<?php
    get_footer();
?>


